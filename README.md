# Java Developer test

## Overview

We want to evaluate your java development skills. As a software development company we need hire the best professionals.

The values we consider more important are:

- To have strong technical basis
- To be delivery oriented, in an agile fashion
- Trust

## Criteria

These criteria are all "should be" but not mandatory:

### For juniors

- Set up a new spring boot and MVC project
- Model clean interfaces for classes and RESTful services. Include swagger online documentation
- Domain java features like OOP, logging, exception modeling and handling, annotations, lambda
- Minimal unit test
- Clean code
- Roughly explain the things

### For seniors

- Full use of design patterns
- Demonstrate SOLID principles
- Mix both relational and non relational databases to adress issues
- Use distributed caching solution
- Use cloud / microservices features like load balancers, service registry and discovery, circuit breakers
- Setup conteinerization and optionally orchestration of containers

## The test

1. Imagine an web application, having a simple jsp page and RESTful interface. The domain and features are of your choice.

2. Implement a page with a minimal action.

3. Implement a minimal REST service for the same domain.

4. Include more features that are necessary to you to demonstrate your skills keeping a user-centered aproach, simulating real world problems.

5. Explain your solution in readme files, javadocs, video, covering as most techniques as you can, under 5 min limit. Let a short and fast run walkthrough for the dummies.

## Score

Each of "Juniors" item count 1, each "Senior" counts 3. Consider each item can be covered minimally, partially, fully or fully & explained, that multiplies by 1, 2, 3 or 4. So the maximum 96 stars are got by (6 + 6 * 3) * 4.

## Deliver

Send your repository link to jobs@bpixel.com.br, with subject Java test, linking video